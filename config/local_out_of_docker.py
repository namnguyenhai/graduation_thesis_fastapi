from config.base import *

ELASTICSEARCH_DSL = {
    'default': {
        'hosts': 'localhost',
        'port': '9200'
    },
}

connections.configure(**ELASTICSEARCH_DSL)
