from config.base import *

ELASTICSEARCH_DSL = {
    'default': {
        'hosts': 'elasticsearch',
        'port': '9200'
    },
}

connections.configure(**ELASTICSEARCH_DSL)
