from collections import defaultdict
from enum import Enum
from typing import List

import spacy
from pydantic import BaseModel

from config.base import NER_model


class EntityName(Enum):
    org = "org"
    customer = "customer"
    item = "item"
    total_price = "total_price"
    address = "address"
    date = "date"


class Entity(BaseModel):
    text: str
    label: str


class NERResult(BaseModel):
    list_entity: List[Entity] = []
    extracted_text: str = None

    def join_the_same_label(self):
        # Gain set of unique label
        set_labels = set(
            map(
                lambda e: e.label,
                self.list_entity
            )
        )

        # Join the texts which have the same label
        list_entity: List[Entity] = []
        for label in set_labels:
            text = " ".join(
                list(
                    map(
                        lambda e: e.text,
                        filter(
                            lambda e: label == e.label,
                            self.list_entity
                        )
                    )
                )
            )
            list_entity.append(
                Entity(text=text, label=label)
            )
        self.list_entity = list_entity


class NERPerformer:

    def __init__(self):
        self._retrained_nlp = spacy.load(NER_model)
        self.results: NERResult = NERResult()

    def perform(self, text: str, cutoff=0.999999):
        def filter_low_confidence_entities(entities, cutoff=cutoff):
            return {key: value for key, value in entities.items() if value > cutoff}

        doc = self._retrained_nlp.make_doc(text)
        beams = self._retrained_nlp.entity.beam_parse([doc], beam_width=16, beam_density=0.0001)
        entity_scores = defaultdict(float)
        total_score = 0
        for score, ents in self._retrained_nlp.entity.moves.get_beam_parses(beams[0]):
            total_score += score
            for start, end, label in ents:
                entity_scores[(start, end, label)] += score

        normalized_beam_score = {dict_key: dict_value / total_score for dict_key, dict_value in entity_scores.items()}
        high_confidence_entities = filter_low_confidence_entities(normalized_beam_score, cutoff)
        high_confidence_entity_texts = {key: doc[int(key[0]): int(key[1])] for key, value in
                                        high_confidence_entities.items()}

        # Get the result
        self.results.extracted_text = text
        for k, v in high_confidence_entity_texts.items():
            self.results.list_entity.append(
                Entity(text=str(v), label=k[2])
            )
        self.results.join_the_same_label()

    def show_result(self):
        print("--------NER results:")
        for r in self.results.list_entity:
            print("{} ===> {}".format(r.text, r.label))
