import re
from threading import Thread

import cv2
import numpy as np
import pytesseract

from service import predictor, measure_duration_processing_sync


class NewPreprocessor:
    def __init__(self, raw_img: np.ndarray, language: str, show_img: bool = False):
        # In the preprocesing progress, would you like to show imgs?
        self._show_img: bool = show_img

        # Lang
        self._language: str = language

        # Images: raw, gray, binary
        self._RAW_IMG: np.ndarray = raw_img
        # self._RGB_img: np.ndarray = deepcopy(raw_img)

    def show_img(self, title: str, img: np.ndarray):
        def inner():
            cv2.namedWindow(title, cv2.WINDOW_NORMAL)
            cv2.imshow(title, img)  # Show image
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        if self._show_img:
            Thread(target=inner).start()

    def preprocess(self) -> np.ndarray:
        foreground = self._perform_segmentation(self._RAW_IMG)
        rotated = self._rotate_properly(foreground)
        return rotated

    @measure_duration_processing_sync
    def _perform_segmentation(self, im: np.ndarray) -> np.array:
        # Predict
        outputs = predictor(im)

        # Clear background
        hobbes_mask = outputs["instances"].pred_masks.to('cpu')[0].numpy()
        mask = np.stack([hobbes_mask, hobbes_mask, hobbes_mask], axis=2)
        mask_indices_0, mask_indices_1, _ = np.where(mask == True)
        min_0: int = mask_indices_0.min()
        max_0: int = mask_indices_0.max()
        min_1: int = mask_indices_1.min()
        max_1: int = mask_indices_1.max()
        foreground = im[min_0:max_0, min_1:max_1]
        self.show_img('foreground', im[min_0:max_0, min_1:max_1])

        return foreground

    @measure_duration_processing_sync
    def _rotate_properly(self, img: np.ndarray):
        def _rotate_image_without_cropping(mat, angle):
            """
            Rotates an image (angle in degrees) and expands image to avoid cropping
            """

            height, width = mat.shape[:2]  # image shape has 3 dimensions
            image_center = (width / 2,
                            height / 2)  # getRotationMatrix2D needs coordinates in reverse order (width, height) compared to shape

            rotation_mat = cv2.getRotationMatrix2D(image_center, angle, 1.)

            # rotation calculates the cos and sin, taking absolutes of those.
            abs_cos = abs(rotation_mat[0, 0])
            abs_sin = abs(rotation_mat[0, 1])

            # find the new width and height bounds
            bound_w = int(height * abs_sin + width * abs_cos)
            bound_h = int(height * abs_cos + width * abs_sin)

            # subtract old image center (bringing image back to origo) and adding the new image center coordinates
            rotation_mat[0, 2] += bound_w / 2 - image_center[0]
            rotation_mat[1, 2] += bound_h / 2 - image_center[1]

            # rotate image with the new bounds and translated rotation matrix
            rotated_mat = cv2.warpAffine(mat, rotation_mat, (bound_w, bound_h))
            return rotated_mat

        # Detect Orientation, Script
        osd = pytesseract.image_to_osd(img)
        angle = re.search('(?<=Rotate: )\d+', osd).group(0)
        # Rotate
        rotated_img: np.ndarray = _rotate_image_without_cropping(img, angle=-float(angle))
        self.show_img('rotate', rotated_img)

        return rotated_img
